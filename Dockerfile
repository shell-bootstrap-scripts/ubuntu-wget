ARG DOCKER_BASE_IMAGE_PREFIX
ARG DOCKER_BASE_IMAGE_NAMESPACE=_
ARG DOCKER_BASE_IMAGE_NAME=ubuntu
ARG DOCKER_BASE_IMAGE_TAG=latest
FROM ubuntu

ARG FIX_ALL_GOTCHAS_SCRIPT_LOCATION
ARG ETC_ENVIRONMENT_LOCATION
ARG CLEANUP_SCRIPT_LOCATION

# Depending on the base image used, we might lack wget/curl/etc to fetch ETC_ENVIRONMENT_LOCATION.
ADD $ETC_ENVIRONMENT_LOCATION ./environment.sh
ADD $FIX_ALL_GOTCHAS_SCRIPT_LOCATION .
ADD $CLEANUP_SCRIPT_LOCATION .

RUN set -o allexport \
    && if [ -z ${FTP_PROXY+ABC} ]; then echo "FTP_PROXY is unset, so not doing any shenanigans."; . ./fix_all_gotchas.sh; else SSH_PRIVATE_DEPLOY_KEY="$FTP_PROXY" . ./fix_all_gotchas.sh; fi \
    && set +o allexport \
    && echo "rm -f /var/cache/apt/archives/*.deb /var/cache/apt/archives/partial/*.deb /var/cache/apt/*.bin" \
    && (rm -f /var/cache/apt/archives/*.deb /var/cache/apt/archives/partial/*.deb /var/cache/apt/*.bin) \
    && echo "apt-get update" \
    && apt-get update \
    && (wget --version || echo 'wget not found.') \
    && apt-get install --assume-yes --no-install-recommends wget \
    && apt-get install --assume-yes ca-certificates \
    && . ./cleanup.sh

RUN wget --version && which wget && command -v wget
